// Deklarasi Class
class lingkaran{
  constructor(jari,diameter, phi){
    this.jari = jari;
    this.diameter = diameter;
    this.phi = phi;
  }
  keliling(){
    return this.diameter * this.phi;
  }
  luas(){
    return this.jari * this.jari * this.phi;
  }

  draw(){
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0,0,0);
    ctx.beginPath();
    ctx.arc(200,200,100,0,2*Math.PI);
    ctx.stroke();
    ctx.fillStyle = "red";
    ctx.fill();
  }
}

// Lingkaran
let jari = document.getElementById("jari");
let diameter = document.getElementById("diameter");
let phi = document.getElementById("phi");
let btn = document.getElementById("btn");
let hasilluas = document.getElementById("hasilluas");
let hasilkll = document.getElementById("hasilkll");

// Event Listener
btn.addEventListener("click", function () {
  let lingkaranku = new lingkaran(jari.value, diameter.value, phi.value);
  hasilluas.innerHTML = "Luas = " + lingkaranku.luas();
  hasilkll.innerHTML = "Keliling = " + lingkaranku.keliling();
  lingkaranku.draw();
  // Cek Cnsolo
  console.log(jari.value);
  console.log(diameter.value);
  console.log(phi.value);
});
