// Deklarasi Class
class Belahketupat {
  constructor(sisi, d1, d2) {
    this.sisi = sisi;
    this.d1 = d1;
    this.d2 = d2;
  }
  keliling() {
    return this.sisi * 4;
  }
  luas() {
    return (this.d1 * this.d2) / 2;
  }
//   gambar belah ketupat di canvas dengan nilai sisi, d1, d2
  draw() {
    let canvas = document.getElementById("canvas");
    let ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0,0,0);
    ctx.beginPath();
    ctx.moveTo(100, 100);
    ctx.lineTo(200, 100);
    ctx.lineTo(300, 200);
    ctx.lineTo(200, 300);
    ctx.lineTo(100, 300);
    ctx.lineTo(0, 200);
    ctx.lineTo(100, 100);
    ctx.stroke();
    ctx.fillStyle = "red";
    ctx.fill();
  }
  
}


// BelahKetupat
let sisi = document.getElementById("sisi");
let d1 = document.getElementById("d1");
let d2 = document.getElementById("d2");
let btn = document.getElementById("btn");
let hasilluas = document.getElementById("hasilluas");
let hasilkll = document.getElementById("hasilkll");

// Event Listener
btn.addEventListener("click", function () {
  let belahketupat = new Belahketupat(sisi.value, d1.value, d2.value);
  hasilluas.innerHTML = "Luas = " + belahketupat.luas();
  hasilkll.innerHTML = "Keliling = " + belahketupat.keliling();
  belahketupat.draw();
  // Cek Cnsolo
  console.log(sisi.value);
  console.log(d1.value);
  console.log(d2.value);
});
