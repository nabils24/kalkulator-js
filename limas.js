// Deklarasi Class
class limas{
    constructor(luas_alas,tinggi, lst){
        this.luas_alas = luas_alas;
        this.tinggi = tinggi;
        this.lst = lst;
    }
    volume(){
        return this.luas_alas * this.tinggi / 3;
    }
    luas_selimut(){
        return this.lst;
    }
    luas_permukaan(){
        return this.luas_alas + this.lst;
    }
    draw(){
        let canvas = document.getElementById("myCanvas");
        let ctx = canvas.getContext("2d");
        ctx.beginPath();
        ctx.moveTo(100, 100);
        ctx.lineTo(200, 100);
        ctx.lineTo(150, 50);
        ctx.closePath();
        ctx.stroke();
        ctx.fillStyle = "red";
        ctx.fill();
    }
    
  }
  
  // limas
    let luas_alas = document.getElementById("luas_alas");
    let tinggi = document.getElementById("tinggi");
    let lst = document.getElementById("lst");
    let btn = document.getElementById("btn");
    let hasillp = document.getElementById("hasillp");
    let hasilvolum = document.getElementById("hasilvolum");

 
  
  // Event Listener
  btn.addEventListener("click", function () {
    let limas1 = new limas(luas_alas.value, tinggi.value, lst.value);
    hasillp.innerHTML = "Luas Permukaan Limas = " + limas1.luas_permukaan();
    hasilvolum.innerHTML = "Volume Limas = " +limas1.volume();
    limas1.draw();
  });
  